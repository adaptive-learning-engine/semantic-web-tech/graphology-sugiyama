import type { AbstractGraph, Attributes } from 'graphology-types';

// eslint-disable-next-line import/no-extraneous-dependencies
export type { Attributes } from 'graphology-types';

export type ChildDir = 'in' | 'out';
export const DEFAULT_CHILD_DIR = 'out';

export type IGraph<
  NodeAttributes extends Attributes = Attributes,
  EdgeAttributes extends Attributes = Attributes,
  GraphAttributes extends Attributes = Attributes,
> = AbstractGraph<NodeAttributes, EdgeAttributes, GraphAttributes>;

export function inverseChildDir(childDir: ChildDir): ChildDir {
  return childDir === 'in' ? 'out' : 'in';
}
