/**
 * Graphology Topological Sort
 * ============================
 *
 * Function performing topological sort over the given DAG using Kahn's
 * algorithm.
 *
 * This function also works on disconnected graphs.
 *
 * [Reference]:
 * https://en.wikipedia.org/wiki/Topological_sorting
 */
import type { Attributes, NodeIterationCallback } from 'graphology-types';
import isGraph from 'graphology-utils/is-graph';
import FixedDeque from 'mnemonist/fixed-deque';

import { DEFAULT_CHILD_DIR, IGraph } from '..';

export function forEachNodeInTopologicalOrder<NodeAttributes extends Attributes = Attributes>(
  graph: IGraph<NodeAttributes>,
  callback: NodeIterationCallback<NodeAttributes>,
  childDir = DEFAULT_CHILD_DIR
) {
  if (!isGraph(graph)) throw new Error('graphology-dag/topological-sort: the given graph is not a valid graphology instance.');

  // NOTE: falsely mixed graph representing directed graphs will work
  if (graph.type === 'undirected' || graph.undirectedSize !== 0)
    throw new Error('graphology-dag/topological-sort: cannot work if graph is not directed.');

  if (graph.multi) throw new Error('graphology-dag/topological-sort: cannot work with multigraphs.');

  type QueueItem = [string, NodeAttributes];
  const queue = new FixedDeque<QueueItem>(Array, graph.order);
  const degrees: Record<string, number> = {};
  // let total = 0;

  graph.forEachNode((node, attr) => {
    const degree = childDir === 'out' ? graph.inDegree(node) : graph.outDegree(node);

    if (degree === 0) {
      queue.push([node, attr]);
    } else {
      degrees[node] = degree;
      // total += degree;
    }
  });

  function neighborCallback(neighbor: string, attr: NodeAttributes) {
    const neighborDegree = --degrees[neighbor];

    // total--;

    if (neighborDegree === 0) queue.push([neighbor, attr]);

    degrees[neighbor] = neighborDegree;

    // NOTE: key deletion is expensive in JS and in this case pointless so
    // we just skip it for performance reasons
  }

  while (queue.size !== 0) {
    const [node, attr] = queue.shift() as QueueItem;

    callback(node, attr);

    // iterate through children
    if (childDir === 'out') graph.forEachOutNeighbor(node, neighborCallback);
    else graph.forEachInNeighbor(node, neighborCallback);
  }

  // if (total !== 0)
  //   throw new Error(
  //     // eslint-disable-next-line spellcheck/spell-checker
  //     "graphology-dag/topological-sort: given graph is not acyclic."
  //   );
}

export function topologicalSort(graph: IGraph, childDir = DEFAULT_CHILD_DIR): string[] {
  if (!isGraph(graph)) throw new Error('graphology-dag/topological-sort: the given graph is not a valid graphology instance.');

  const sortedNodes = new Array(graph.order);
  let i = 0;

  forEachNodeInTopologicalOrder(
    graph,
    (node) => {
      sortedNodes[i++] = node;
    },
    childDir
  );

  return sortedNodes;
}
