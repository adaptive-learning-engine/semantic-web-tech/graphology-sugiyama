import { IGraph } from '.';

export interface IGetRootOptions {
  childDir: 'in' | 'out';
}

export function getRoots(graph: IGraph, options?: IGetRootOptions): string[] {
  const results: string[] = [];

  // if childDir is in, then parents are out
  const mode = options?.childDir || 'out';

  const cbIn = (node: string) => {
    const degree = graph.outDegree(node);
    if (degree === 0) results.push(node);
  };

  const cbOut = (node: string) => {
    const degree = graph.inDegree(node);
    if (degree === 0) results.push(node);
  };

  const cb = mode === 'out' ? cbOut : cbIn;

  graph.forEachNode(cb);

  return results;
}
