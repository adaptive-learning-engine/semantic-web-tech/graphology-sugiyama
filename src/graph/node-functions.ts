import { ChildDir, DEFAULT_CHILD_DIR, IGraph } from '.';

export function getNodeParents(graph: IGraph, node: string, childDir: ChildDir = DEFAULT_CHILD_DIR): string[] {
  return childDir === 'in' ? graph.outboundNeighbors(node) : graph.inboundNeighbors(node);
}

export function forEachNodeParent(
  graph: IGraph,
  node: string,
  cb: (childKey: string) => void,
  childDir: ChildDir = DEFAULT_CHILD_DIR
): void {
  return childDir === 'in' ? graph.forEachOutboundNeighbor(node, cb) : graph.forEachInboundNeighbor(node, cb);
}

export function getNodeChildren(graph: IGraph, node: string, childDir: ChildDir = DEFAULT_CHILD_DIR): string[] {
  return childDir === 'in' ? graph.inboundNeighbors(node) : graph.outboundNeighbors(node);
}

export function forEachNodeChild(
  graph: IGraph,
  node: string,
  cb: (childKey: string) => void,
  childDir: ChildDir = DEFAULT_CHILD_DIR
): void {
  return childDir === 'in' ? graph.forEachInboundNeighbor(node, cb) : graph.forEachOutboundNeighbor(node, cb);
}
