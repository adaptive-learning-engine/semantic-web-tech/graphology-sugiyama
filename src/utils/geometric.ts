export interface ISize {
  height: number;
  width: number;
}

export interface IPosition {
  x: number;
  y: number;
}
