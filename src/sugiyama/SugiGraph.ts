import { Attributes, IGraph } from '../graph';
import { IPosition, ISize } from '../utils/geometric';

export interface ISugiNodeAttrs extends Partial<IPosition> {
  layer?: number;
  size?: ISize;
  x?: number;
  y?: number;
}

export interface ISugiLayer {
  layer: number;
  size: ISize;
  nodes: string[];
}

export interface ISugiGraphAttrs {
  layers?: ISugiLayer[];
  maxLayer?: number;
  size: ISize;
}

export type ISugiGraph = IGraph<ISugiNodeAttrs, Attributes, ISugiGraphAttrs>;
