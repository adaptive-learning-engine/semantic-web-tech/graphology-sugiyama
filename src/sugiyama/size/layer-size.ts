import { ISugiGraph } from '../SugiGraph';

export function addLayerHeights(sugiGraph: ISugiGraph) {
  const layers = sugiGraph.getAttributes().layers;

  if (!layers) throw new Error('`layers` not existent on the SugiGraph');

  layers.forEach((layer) => {
    const nodeHeights = layer.nodes.map((node) => {
      const size = sugiGraph.getNodeAttribute(node, 'size');
      if (!size) throw new Error(`Node '${node}' of SugiGraph has no associated size`);
      return size.height;
    });
    layer.size.height = Math.max(...nodeHeights);
  });
}

export function addLayerWidths(sugiGraph: ISugiGraph) {
  const layers = sugiGraph.getAttributes().layers;

  if (!layers) throw new Error('`layers` not existent on the SugiGraph');

  layers.forEach((layer) => {
    const width = layer.nodes.reduce((prevWidth, node) => {
      const size = sugiGraph.getNodeAttribute(node, 'size');
      if (!size) throw new Error(`Node '${node}' of SugiGraph has no associated size`);
      return prevWidth + size.width;
    }, 0);
    layer.size.width = width;
  });
}
