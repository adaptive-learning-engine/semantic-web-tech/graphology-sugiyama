import { ISugiGraph } from '../SugiGraph';

export function setNodesXFromCenter(sugiGraph: ISugiGraph): void {
  const layers = sugiGraph.getAttributes().layers;

  if (!layers) throw new Error('`layers` not existent on the SugiGraph');

  const maxWidth = Math.max(...layers.map((layer) => layer.size.width));

  for (let iLayer = 0; iLayer < layers.length; ++iLayer) {
    const layer = layers[iLayer];
    let width = 0;
    const offset = (maxWidth - layer.size.width) / 2;
    for (let iNode = 0; iNode < layer.nodes.length; ++iNode) {
      const nodeAttrs = sugiGraph.getNodeAttributes(layer.nodes[iNode]);
      const nodeWidth = nodeAttrs.size.width;
      nodeAttrs.x = width + nodeWidth / 2 + offset;
      width += nodeWidth;
    }
  }
}
