import { ISugiGraph } from '../SugiGraph';

export function setNodesYFromLayers(sugiGraph: ISugiGraph) {
  const layers = sugiGraph.getAttributes().layers;

  if (!layers) throw new Error('`layers` not existent on the SugiGraph');

  let height = 0;
  for (let iLayer = 0; iLayer < layers.length; ++iLayer) {
    const layer = layers[iLayer];
    const layerHeight = layer.size.height;
    for (let iNode = 0; iNode < layer.nodes.length; ++iNode) {
      const nodeAttrs = sugiGraph.getNodeAttributes(layer.nodes[iNode]);
      nodeAttrs.y = height + layerHeight / 2;
    }
    height += layerHeight;
  }
  sugiGraph.getAttributes().size.height = height;
}
