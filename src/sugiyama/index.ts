import Graph from 'graphology';

import { Attributes, ChildDir, IGraph } from '../graph';
import { IPosition, ISize } from '../utils/geometric';

import { ISugiGraph, ISugiGraphAttrs, ISugiLayer, ISugiNodeAttrs } from './SugiGraph';
import { setNodesXFromCenter } from './coord/center';
import { setNodesYFromLayers } from './coord/from-layers';
import { ILayeringOptions } from './layering';
import { longestPath } from './layering/longest-path';
import { addLayerHeights, addLayerWidths } from './size/layer-size';

type GetNodeSize<NodeAttributes extends Attributes> = (node: string, attr: NodeAttributes) => ISize;

export function toSugiGraph<NodeAttributes extends Attributes>(graph: IGraph<NodeAttributes>): ISugiGraph {
  const copy = new Graph<ISugiNodeAttrs, Attributes, ISugiGraphAttrs>();
  copy.setAttribute('size', { width: 0, height: 0 });

  graph.forEachNode((node) => {
    copy.addNode(node);
  });

  graph.forEachEdge((key, attr, source, target) => {
    copy.addEdgeWithKey(key, source, target);
  });

  return copy;
}

export function addLayers(sugiGraph: ISugiGraph, layers: Map<string, number>): void {
  const graphAttrs = sugiGraph.getAttributes();
  graphAttrs.layers = createSugiLayers(layers);
  graphAttrs.maxLayer = graphAttrs.layers.length - 1;

  sugiGraph.forEachNode((node, attrs) => {
    // TODO: throw exception when no layer is given
    attrs.layer = layers.get(node) as number;
  });
}

export function addNodeSizes<NodeAttributes extends Attributes>(
  graph: IGraph<NodeAttributes>,
  sugiGraph: ISugiGraph,
  getNodeSize: GetNodeSize<NodeAttributes>
): void {
  sugiGraph.forEachNode((node, attrs) => {
    const origAttrs = graph.getNodeAttributes(node);
    attrs.size = getNodeSize(node, origAttrs);
  });
}

export function createSugiLayers(layers: Map<string, number>): ISugiLayer[] {
  const maxLayer = Math.max(...layers.values());

  const result: ISugiLayer[] = [];
  for (let i = 0; i <= maxLayer; ++i) result.push({ layer: i, size: { width: 0, height: 0 }, nodes: [] });

  layers.forEach((layer, node) => {
    result[layer].nodes.push(node);
  });

  return result;
}

export interface ISugiyamaOptions<N extends Attributes> {
  childDir?: ChildDir;
  layering?: ILayeringOptions;
  getNodeSize: GetNodeSize<N>;
}

export function sugiyama<N extends Attributes, L extends Attributes>(graph: IGraph<N, L>, options: ISugiyamaOptions<N>): ISugiGraph {
  const sugiGraph = toSugiGraph(graph);
  addNodeSizes(graph, sugiGraph, options.getNodeSize);

  // compute layers
  let layeringOptions = options.layering;
  if (options.childDir) {
    if (!layeringOptions) layeringOptions = { childDir: options.childDir };
    else if (!layeringOptions.childDir) layeringOptions = { ...layeringOptions, childDir: options.childDir };
  }
  const layers = longestPath(graph, layeringOptions);
  addLayers(sugiGraph, layers);
  addLayerHeights(sugiGraph);
  addLayerWidths(sugiGraph);

  setNodesYFromLayers(sugiGraph); // requires layer heights
  setNodesXFromCenter(sugiGraph); // requires layer widths

  // create layers
  // const layers = sugify(dag);

  // cache and split node sizes
  // TODO: reimplement
  // const [xSize, ySize] = cachedNodeSize(options.sugiNodeSize);

  // verify height
  // const height = sugiGraph.getAttributes().size.height;
  // if (height <= 0) {
  //   throw new Error('at least one node must have positive height, but total height was zero');
  // }

  // minimize edge crossings
  // TODO: decross
  // options.decross(layers);

  // assign coordinates
  // let width = options.coord(layers, xSize);

  // verify
  // verifyCoordAssignment(layers, width);

  // // scale x
  // if (options.size !== null) {
  //   const [newWidth, newHeight] = options.size;
  //   scaleLayers(layers, newWidth / width, newHeight / height);
  //   width = newWidth;
  //   height = newHeight;
  // }

  // Update original dag with values
  // unsugify(layers);

  // layout info
  return sugiGraph;
}

export function positionsFromSugiGraph(sugiGraph: ISugiGraph): Record<string, IPosition> {
  const result: Record<string, IPosition> = {};
  sugiGraph.forEachNode((node, attrs) => {
    result[node] = { x: attrs.x, y: attrs.y };
  });
  return result;
}
