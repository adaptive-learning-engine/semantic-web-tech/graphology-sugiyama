/**
 * A {@link LongestPathOperator} that minimizes the height of the final layout
 *
 * @packageDocumentation
 */
import { DEFAULT_CHILD_DIR, IGraph, inverseChildDir } from '../../graph';
import { getNodeParents } from '../../graph/node-functions';
import { forEachNodeInTopologicalOrder } from '../../graph/traversal/topological';

import { ILayeringOptions } from './options';

export type ILongestPathOptions = ILayeringOptions;

export function longestPath(graph: IGraph, options?: ILongestPathOptions): Map<string, number> {
  let childDir = options?.childDir || DEFAULT_CHILD_DIR;
  if (options?.inverseChildDir) childDir = inverseChildDir(childDir);
  const inverse = options?.inverseLayers;

  const layers: Map<string, number> = new Map();

  if (graph.order === 0) return layers;

  forEachNodeInTopologicalOrder(
    graph,
    (node) => {
      const parents = getNodeParents(graph, node, childDir);
      const parentRanks = parents.map((parent) => layers.get(parent) || 0);
      layers.set(node, parents.length === 0 ? 0 : Math.max(...parentRanks) + 1);
    },
    childDir
  );

  if (inverse) {
    const max = Math.max(...layers.values());
    layers.forEach((value, key) => {
      layers.set(key, max - value);
    });
  }

  return layers;
}
