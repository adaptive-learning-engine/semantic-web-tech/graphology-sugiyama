import { ChildDir } from '../../graph';

export interface ILayeringOptions {
  childDir?: ChildDir;
  inverseChildDir?: boolean; // TODO: as sugiyama option?
  inverseLayers?: boolean;
}
